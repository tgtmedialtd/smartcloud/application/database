const core = require(`@smartcloudai/core`).core
import { Api, Init } from "./init"


process.on('message', function (emit) {
   switch(emit.topic) {

     case "startup":
      break;

     case "restart":
      break;

     case "shutdown":
      break;

     case "api":
      new Api(emit.data.message)
      break;
   }
})

function initialize() {
  core.pm.sendDataToProcessId(0, { type : 'process:msg', data : {app: process.env.name.split(" ~ ")[1], id: process.env.pm_id}, topic: "extensionLoaded" }, function(err: any) { if (err) console.log(err); });
  new Init()
}

initialize()
